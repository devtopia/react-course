import React from "react";

interface IModal {
  onCancel: () => void;
  onConfirm: () => void;
}

const Modal: React.FC<IModal> = props => {
  return (
    <>
      <div className="modal">
        <p>Are you sure?</p>
        <button className="btn btn--alt" onClick={props.onCancel}>
          Cancel
        </button>
        <button className="btn" onClick={props.onConfirm}>
          Confirm
        </button>
      </div>
    </>
  );
};

export default Modal;
