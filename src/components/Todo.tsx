import React, { useState } from "react";

import BackDrop from "./BackDrop";
import Modal from "./Modal";

interface ITodo {
  title: string;
}

const Todo: React.FC<ITodo> = props => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const deleteHandler = () => {
    setModalIsOpen(true);
  };
  const closeModalHandler = () => {
    setModalIsOpen(false);
  };
  return (
    <>
      <div className="card">
        <h2>{props.title}</h2>
        <div className="actions">
          <button className="btn" onClick={deleteHandler}>
            Delete
          </button>
        </div>
      </div>
      {modalIsOpen && (
        <Modal onCancel={closeModalHandler} onConfirm={closeModalHandler} />
      )}
      {modalIsOpen && <BackDrop onCancel={closeModalHandler} />}
    </>
  );
};

export default Todo;
