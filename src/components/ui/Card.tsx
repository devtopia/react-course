import React, { ReactNode } from "react";

import classes from "./Card.module.css";

interface ICard {
  children: ReactNode;
}

const Card: React.FC<ICard> = props => {
  return <div className={classes.card}>{props.children}</div>;
};

export default Card;
