import React, { useRef } from "react";

import Card from "../ui/Card";
import { IMeetupItem } from "./MeetupItem";
import classes from "./NewMeetupForm.module.css";

interface INewMeetupForm {
  onAddMeetup: (meetupData: Omit<IMeetupItem, "id">) => void;
}

const NewMeetupForm: React.FC<INewMeetupForm> = props => {
  // https://github.com/DefinitelyTyped/DefinitelyTyped/issues/35572#issuecomment-493942129
  const titleInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const imageInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const addressInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const descriptionInputRef =
    useRef() as React.MutableRefObject<HTMLTextAreaElement>;
  const submitHandler = (event: React.SyntheticEvent) => {
    event.preventDefault();
    const enteredTitle = titleInputRef.current.value;
    const enteredImage = imageInputRef.current.value;
    const enteredAddress = addressInputRef.current.value;
    const enteredDescription = descriptionInputRef.current.value;
    const meetupData = {
      title: enteredTitle,
      image: enteredImage,
      address: enteredAddress,
      description: enteredDescription
    };
    console.log(meetupData);
    props.onAddMeetup(meetupData);
  };
  return (
    <Card>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor="title">Meetup Title</label>
          <input type="text" required id="title" ref={titleInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="image">Meetup Image</label>
          <input type="url" required id="image" ref={imageInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="address">Address</label>
          <input type="text" required id="address" ref={addressInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="description">Description</label>
          <textarea
            required
            id="description"
            rows={5}
            ref={descriptionInputRef}
          />
        </div>
        <div className={classes.actions}>
          <button>Add Meetup</button>
        </div>
      </form>
    </Card>
  );
};

export default NewMeetupForm;
