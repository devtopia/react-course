import React from "react";

import MeetupItem, { IMeetupItem } from "./MeetupItem";
import classes from "./MeetupList.module.css";

interface IMeetupList {
  meetups: IMeetupItem[];
}

const MeetupList: React.FC<IMeetupList> = props => {
  return (
    <ul className={classes.list}>
      {props.meetups.map(meetup => (
        <MeetupItem
          key={meetup.id}
          id={meetup.id}
          title={meetup.title}
          image={meetup.image}
          address={meetup.address}
          description={meetup.description}
        />
      ))}
    </ul>
  );
};

export default MeetupList;
