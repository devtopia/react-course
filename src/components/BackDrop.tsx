import React from "react";

interface IBackDrop {
  onCancel: () => void;
}

const BackDrop: React.FC<IBackDrop> = props => {
  return <div className="backdrop" onClick={props.onCancel}></div>;
};

export default BackDrop;
