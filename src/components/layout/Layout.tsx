import React, { ReactNode } from "react";

import classes from "./Layout.module.css";
import MainNavigation from "./MainNavigation";

interface ILayout {
  children: ReactNode;
}

const Layout: React.FC<ILayout> = props => {
  return (
    <div>
      <MainNavigation />
      <main className={classes.main}>{props.children}</main>
    </div>
  );
};

export default Layout;
