import React from "react";
import { useHistory } from "react-router-dom";

import { IMeetupItem } from "../components/meetups/MeetupItem";
import NewMeetupForm from "../components/meetups/NewMeetupForm";

const NewMeetupPage = () => {
  const history = useHistory();
  const addMeetupHandler = (meetupData: Omit<IMeetupItem, "id">) => {
    fetch(
      "https://react-course-834c0-default-rtdb.asia-southeast1.firebasedatabase.app/meetups.json",
      {
        method: "POST",
        body: JSON.stringify(meetupData),
        headers: {
          "Content-Type": "application/json"
        }
      }
    ).then(() => {
      history.replace("/");
    });
  };
  return (
    <section>
      <h1>New Meetup Page</h1>
      <NewMeetupForm onAddMeetup={addMeetupHandler} />
    </section>
  );
};

export default NewMeetupPage;
