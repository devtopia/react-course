import React, { createContext, useState } from "react";

import { IMeetupItem } from "../components/meetups/MeetupItem";

type FavoriteContextType = {
  favorites: IMeetupItem[];
  totalFavorites: number;
  addFavorite: (favoriteMeetup: IMeetupItem) => void;
  removeFavorite: (meetupId: string) => void;
  itemIsFavorite: (meetupId: string) => boolean;
};
const FavoritesContext = createContext<FavoriteContextType>(
  {} as FavoriteContextType
);

export const FavoriteContextProvider: React.FC<{
  children: React.ReactNode;
}> = props => {
  const [userFavorites, setUserFavorites] = useState<IMeetupItem[]>([]);
  const addFavoriteHandler = (favoriteMeetup: IMeetupItem) => {
    setUserFavorites(prevUserFavorites => {
      return prevUserFavorites.concat(favoriteMeetup);
    });
  };
  const removeFavoriteHandler = (meetupId: string) => {
    setUserFavorites(prevUserFavorites => {
      return prevUserFavorites.filter(meetup => meetup.id !== meetupId);
    });
  };
  const itemIsFavoriteHandler = (meetupId: string) => {
    return userFavorites.some(meetup => meetup.id === meetupId);
  };
  const context = {
    favorites: userFavorites,
    totalFavorites: userFavorites.length,
    addFavorite: addFavoriteHandler,
    removeFavorite: removeFavoriteHandler,
    itemIsFavorite: itemIsFavoriteHandler
  };
  return (
    <FavoritesContext.Provider value={context}>
      {props.children}
    </FavoritesContext.Provider>
  );
};

export default FavoritesContext;
