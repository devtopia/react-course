/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "jest-environment-jsdom",
  testPathIgnorePatterns: ["<rootDir>/.next", "<rootDir>/node_modules"],
  moduleNameMapper: {
    "\\.(css)$": "<rootDir>/node_modules/jest-css-modules"
  },
  transform: {
    "^.+\\.tsx?$": "babel-jest"
  }
};
