## Overview

このサンプルはMaximilianさんのUdemyの講座に出たサンプルをTypeScriptで作成しました。
ESLintでエラーが出ないように修正したため、JavaScriptで作成された講座のサンプルとはかなり違いがあります。

## How to set up the app

packageをインストールする。

```bash
yarn install
```

yarnがインストールされていない場合は、先にyarnをインストールする。

```bash
npm install -g yarn
```

devサーバーを起動する。

```bash
yarn start
```

ブラウザで確認する。

http://localhost:3000

![ReactMeetups.gif](./public/ReactMeetups.gif)
